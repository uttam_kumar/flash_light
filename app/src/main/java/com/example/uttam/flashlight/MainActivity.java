package com.example.uttam.flashlight;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static final String CAMERA_FRONT = "1";
    public static final String CAMERA_BACK = "0";

    private String cameraId = CAMERA_BACK;
    private boolean isFlashSupported;
    private boolean isTorchOn;

    Button captureBtn;
    Button frontFlashBtn,backFlashBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//
//        captureBtn = (Button) findViewById(R.id.flashBtnOnId);
//        // Listener for Flash on/off button
//        captureBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                capture();
//                //actionFlash();
//            }
//        });

        /* First check if device is supporting flashlight or not */
        isFlashSupported = this.getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);

        frontFlashBtn = (Button) findViewById(R.id.frontFlashBtnId);
        // Listener for Flash on/off button
        frontFlashBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frontFlash();
            }
        });

        backFlashBtn = (Button) findViewById(R.id.backFlashBtnId);
        // Listener for Flash on/off button
        backFlashBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backFlash();
            }
        });

    }

//    private void capture() {
//        onFlash();
//        try {
//            Thread.sleep(300);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        offFlash();
//        try {
//            Thread.sleep(100);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//        onFlash();
//        try {
//            Thread.sleep(1000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        offFlash();
//    }

//    private void onFlash() {
//        /* First check if device is supporting flashlight or not */
//        isFlashSupported = this.getApplicationContext().getPackageManager()
//                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
//
//        if (isFlashSupported){  // the device support flash
//            CameraManager mCameraManager = (CameraManager) this.getSystemService(Context.CAMERA_SERVICE);
//            try {
//                String mCameraId = mCameraManager.getCameraIdList()[0];
//
//                Log.d("TAG", "actionFlash: "+cameraId);
//                if (mCameraId.equals("0")) {    // currently on back camera
//
//                    try {
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                            mCameraManager.setTorchMode(mCameraId, true);
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            } catch (CameraAccessException e) {
//                Toast.makeText(this, "Cannot access the camera.", Toast.LENGTH_SHORT).show();
//                this.finish();
//            }
//        }
//    }
//
//    private void offFlash() {
//        /* First check if device is supporting flashlight or not */
//        isFlashSupported = this.getApplicationContext().getPackageManager()
//                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
//
//        if (isFlashSupported){  // the device support flash
//            CameraManager mCameraManager = (CameraManager) this.getSystemService(Context.CAMERA_SERVICE);
//            try {
//                String mCameraId = mCameraManager.getCameraIdList()[0];
//
//                Log.d("TAG", "actionFlash: "+cameraId);
//                if (mCameraId.equals("0")) {    // currently on back camera
//
//                    try {
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                            mCameraManager.setTorchMode(mCameraId, false);
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            } catch (CameraAccessException e) {
//                Toast.makeText(this, "Cannot access the camera.", Toast.LENGTH_SHORT).show();
//                this.finish();
//            }
//        }
//    }


    private void  frontFlash() {

        if (isFlashSupported){
            CameraManager mCameraManager = (CameraManager) this.getSystemService(Context.CAMERA_SERVICE);
            try {
                String mCameraId = mCameraManager.getCameraIdList()[1];

                Log.d("TAG", "actionFlash: "+cameraId);
                if (mCameraId.equals("1")) {    // currently on back camera
                    if (!isTorchOn) {
                        try {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                mCameraManager.setTorchMode(mCameraId, true);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(this, "no front flash", Toast.LENGTH_SHORT).show();
                        }

                        isTorchOn = true;

                        frontFlashBtn.setText("front flash off");

                    } else {
                        try {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                mCameraManager.setTorchMode(mCameraId, false);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(this, "no front flash", Toast.LENGTH_SHORT).show();
                        }

                        isTorchOn = false;
                        frontFlashBtn.setText("front flash on");
                    }
                }
            } catch (CameraAccessException e) {
                Toast.makeText(this, "Cannot access front camera.", Toast.LENGTH_SHORT).show();
                this.finish();
            }
        }
    }

    private void  backFlash() {

        if (isFlashSupported){
            CameraManager mCameraManager = (CameraManager) this.getSystemService(Context.CAMERA_SERVICE);
            try {
                String mCameraId = mCameraManager.getCameraIdList()[0];

                Log.d("TAG", "actionFlash: "+cameraId);
                if (mCameraId.equals("0")) {    // currently on back camera
                    if (!isTorchOn) {
                        try {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                mCameraManager.setTorchMode(mCameraId, true);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        isTorchOn = true;
                        backFlashBtn.setText("back flash off");

                    } else {
                        try {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                mCameraManager.setTorchMode(mCameraId, false);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        isTorchOn = false;
                        backFlashBtn.setText("back flash on");
                    }
                }
            } catch (CameraAccessException e) {
                Toast.makeText(this, "Cannot access back camera.", Toast.LENGTH_SHORT).show();
                this.finish();
            }
        }
    }
}
